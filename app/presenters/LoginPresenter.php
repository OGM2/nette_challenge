<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;


class LoginPresenter extends Nette\Application\UI\Presenter
{
	public function createComponentLoginForm()
	{
		$form = new Form;

		$form->addText('username', 'Uživatelské jméno:')
			->setRequired('Vyplňte uživatelské jméno.');

		$form->addPassword('password', 'Heslo:')
			->setRequired('Vyplňte své heslo.');

		$form->addSubmit('send', 'Přihlásit');

		$form->onSuccess[] = [$this, 'loginFormSucceeded'];
		
		return $form;
	}

	public function loginFormSucceeded(Form $form, $values)
	{
		try
		{
			$this->getUser()->login($values->username, $values->password);
			$this->redirect('Admin:');
		}
		catch(Nette\Security\AuthenticationException $e)
		{
			$form->addError('Zadali jste špatné přihlašovací jméno nebo heslo.');
		}
	}

	public function actionLogout()
	{
		$this->getUser()->logout();
		$this->flashMessage('Odhlášení bylo úspěšné.');
		$this->redirect('Homepage:');
	}
	
}
<?php

namespace App\Presenters;

use Nette;
use Nette\Database\Context;
use App\Model\CategoryManager;
use App\Model\ProductManager;
use Nette\Application\UI\Form;
use Nette\Mail\Message;
use Nette\Mail\SendmailMailer;


final class HomepagePresenter extends Nette\Application\UI\Presenter
{
	private $database;
	
	private $categoryManager;
	
	private $productManager;

	public $categoryTree;

	public function __construct(Context $database, CategoryManager $categoryManager, productManager $productManager)
	{
		$this->database = $database;
		$this->categoryManager = $categoryManager;
		$this->productManager = $productManager;
	}

	public function renderDefault()
	{
		$this->getCategories();
		
		$this->template->categories = $this->categoryTree;
		$this->template->products = $this->productManager->getFeaturedProducts();
	}

	public function renderCategory($slug)
	{
		$this->getCategories();
		
		$this->template->categories = $this->categoryTree;
		$this->template->categoryProducts = $this->categoryManager->getCategoryList($slug);
		$this->template->categoryName = $this->categoryManager->getCategoryName($slug);
	}

	public function renderDetail($id)
	{
		$this->getCategories();
		
		$this->template->categories = $this->categoryTree;
		$this->template->product = $this->productManager->getProduct($id);
	}

	public function getCategories()
	{
		$categories = $this->categoryManager->getHiearchy();

		$tree = $this->parseTree($categories, 0);

		$categoryTree = $this->getCategoryTree($tree);
	}

	public function parseTree($categories, $root = null) 
	{
		$return = array();

		foreach($categories as $child => $parent) 
		{

			if($parent['parent'] == $root) 
			{
				unset($categories[$child]);
				
				$return[] = array('name' => $parent['name'],
					'slug' => $parent['slug'],
					'children' => $this->parseTree($categories, $child));
			}
		}
		return empty($return) ? null : $return;    
	}

	public function getCategoryTree($categories)
	{
		if(!is_null($categories) && count($categories) > 0) 
		{
			$this->categoryTree .= '<ol>'."\n";
			
			foreach($categories as $node) 
			{
				$this->categoryTree .= '<li>'."\n".'<a href="'.$this->link('Homepage:category', $node['slug']).'" title="'.$node['name'].'">'.$node['name'].'</a>'."\n";
				
				$this->getCategoryTree($node['children']);
				$this->categoryTree .= '</li>'."\n";
			}
			$this->categoryTree .= '</ol>'."\n";
		}
		return $this->categoryTree;
	}

	public function createComponentQuestionForm()
	{
		$form = new Form;

		$form->addText('name', 'Vaše jméno:*')
		->setRequired();

		$form->addText('phone', 'Váš telefon:*')
		->addRule(form::INTEGER, 'Váš telefon zadejte pouze číslicemi bez předvolby')
		->setRequired();

		$form->addEmail('email', 'Váš e-mail:*')
		->setRequired();

		$form->addTextArea('question', 'Váš dotaz:*')
		->setRequired();

		$form->addSubmit('send', 'Odeslat dotaz');

		$form->onSuccess[] = [$this, 'questionFormSucceeded'];

		return $form;
	}

	public function questionFormSucceeded(Form $form, $values)
	{
		$mail = new Message;
		$mail->setFrom($values->email)
			->addTo('dytrych@webovy-servis.cz')
			->setSubject('Dotaz z formuláře')
			->setBody($values->question);

		$mailer = new SendmailMailer;
		$mailer->send($mail);

		$question = $this->database->table('questions')->insert($values);

		$this->flashMessage('Dotaz byl úspěšně odeslán.', 'success');
		$this->redirect('Homepage:');
	}
}

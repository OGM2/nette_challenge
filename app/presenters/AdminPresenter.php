<?php

namespace App\Presenters;

use Nette;
use App\Model\ProductManager;
use Nette\Database\Context;
use Nette\Application\UI\Form;
use Nette\Utils\Strings;


class AdminPresenter extends Nette\Application\UI\Presenter
{
	private $database;
	
	private $productManager;

	public function __construct(Context $database, ProductManager $productManager)
	{
		$this->database = $database;
		$this->productManager = $productManager;
	}

	public function renderDefault()
	{
		if (!$this->getUser()->isLoggedIn()) 
		{
			$this->flashMessage('Pro vstup do administrace se musíte přihlásit.', 'error');
			$this->redirect('Login:');
		}

		$this->template->products = $this->database->table('products');
	}

	public function renderSearch($search)
	{
		if (!$this->getUser()->isLoggedIn()) 
		{
			$this->flashMessage('Pro vstup do administrace se musíte přihlásit.', 'error');
			$this->redirect('Login:');
		}

		$this->template->products = $this->productManager->searchProducts($search);
	}

	public function actionUpdate($productId)
	{
		if (!$this->getUser()->isLoggedIn()) 
		{
			$this->flashMessage('Pro vstup do administrace se musíte přihlásit.', 'error');
			$this->redirect('Login:');
		}

		$product = $this->database->table('products')->get($productId);

		if(!$product)
		{
			$this->error('Takový produkt v databázi nemáme.');
		}

		$this['productForm']->setDefaults($product->ToArray());
	}

	public function actionDelete($productId)
	{
		if (!$this->getUser()->isLoggedIn()) 
		{
			$this->flashMessage('Pro vstup do administrace se musíte přihlásit.', 'error');
			$this->redirect('Login:');
		}

		$product = $this->database->table('products')
			->where('id', $productId)
			->delete();

		$this->flashMessage('Produkt byl smazán', 'success');
		$this->redirect('Admin:');
	}

	public function createComponentProductForm()
	{
		$selectArr = [];

		$categories = $this->database->table('categories');
		
		foreach($categories as $key => $value) 
		{
			$selectArr[$key] = $value->name;
		}
		
		$form = new Form;

		$form->addText('name', 'Název produktu:*')
			->setRequired();


		$form->addTextArea('description', 'Popis:*')
			->setRequired();

		$form->addText('price', 'Cena produktu (halíře oddělte tečkou):*')
			->setRequired()
			->addRule(Form::PATTERN, 'Koruny od halířů oddělte tečkou a použíjte pouze číslice.', '[0-9]+([.][0-9]{1,2})?');

		$form->addText('color', 'Zadejte barvu produktu:');
		
		$form->addSelect('category_id', 'Vyberte kategorii produktu:', $selectArr)
			->setRequired();
		
		$form->addSelect('active', 'Publikovat:*', [
			1 => 'Ano',
			0 => 'Ne',
		]);

		$form->addSubmit('send', 'Uložit produkt');

		$form->onSuccess[] = [$this, 'productFormSucceeded'];

		return $form;
	}

	public function productFormSucceeded(Form $form, $values)
	{
		if (!$this->getUser()->isLoggedIn()) 
		{
			$this->flashMessage('Pro vstup do administrace se musíte přihlásit.', 'error');
			$this->redirect('Login:');
		}

		$productId = $this->getParameter('productId');

		if($productId)
		{
			$product = $this->database->table('products')->get($productId);
			$product->update($values);

			$this->flashMessage('Produkt byl upraven.', 'success');
		}
		else
		{
			$product = $this->database->table('products')->insert([
				'name' => $values->name,
				'description' => $values->description,
				'slug' => Strings::webalize($values->name),
				'price' => $values->price,
				'color' => !empty($values->color) ? $values->color : null,
				'image' => 'placeholder.png',
				'active' => $values->active,
				'created_at' => date('Y-m-d H:i:s'),
				'category_id' => $values->category_id,
			]);
			
			$this->flashMessage('Produkt byl vytvořen.', 'success');
		}

		$this->redirect('Admin:');
	}

	public function createComponentSearchForm()
	{
		$form = new Form;

		$form->addText('q', 'Vyhledávání:')
			->setRequired();

		$form->addSubmit('send', 'Vyhledat');

		$form->onSuccess[] = [$this, 'searchFormSucceeded'];

		return $form;
	}

	public function searchFormSucceeded(Form $form, $values)
	{
		if (!$this->getUser()->isLoggedIn()) 
		{
			$this->flashMessage('Pro vstup do administrace se musíte přihlásit.', 'error');
			$this->redirect('Login:');
		}

		$this->redirect('Admin:search', $values->q);
	}
}
<?php

namespace App\Model;

use Nette;

class CategoryManager 
{
	use Nette\SmartObject;

	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	public function getHiearchy()
	{
		$categories = $this->database->table('categories');

		$categoriesArr = [];

		foreach($categories as $category)
		{
			$categoriesArr[$category->id] = array(
				'parent' => $category->parent,
				'name' => $category->name,
				'slug' => $category->slug,
			);
		}

		return $categoriesArr;
	}

	public function getCategoryList($slug)
	{
		return $categoryList = $this->database->table('categories')
			->select('categories.name AS cname')
			->select(':products.id')
			->select(':products.name')
			->select(':products.description')
			->select(':products.slug')
			->select(':products.price')
			->select(':products.image')
			->select(':products.color')
			->where('categories.slug = ? AND :products.active = ?', $slug, true);
	}

	public function getCategoryName($slug)
	{
		return $categoryList = $this->database->table('categories')
			->where('categories.slug = ?', $slug)->fetch();
	}
}
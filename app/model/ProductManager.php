<?php

namespace App\Model;

use Nette;

class ProductManager 
{
	use Nette\SmartObject;

	private $database;

	public function __construct(Nette\Database\Context $database)
	{
		$this->database = $database;
	}

	public function searchProducts($search)
	{
		return $this->database->table('products')
			->where('name LIKE ?', '%'.$search.'%');
	}

	public function getProduct($id)
	{
		return $this->database->table('products')
			->get($id);
	}

	public function getFeaturedProducts()
	{
		return $this->database->table('products')
			->where('image != ?', 'placeholder.png')
			->where('active = ?', true);
	}

}
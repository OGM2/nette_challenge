-- Adminer 3.6.2 MySQL dump

SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `parent` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `categories` (`id`, `name`, `parent`, `slug`, `date_created`) VALUES
(1,	'Hudba',	'0',	'hudba',	'2018-11-11 18:20:19'),
(2,	'Nosiče',	'1',	'nosice',	'2018-11-11 18:20:27'),
(3,	'CD',	'2',	'cd',	'2018-11-11 18:20:30'),
(4,	'LP',	'2',	'lp',	'2018-11-11 18:20:33'),
(5,	'Knihy',	'0',	'knihy',	'2018-11-12 13:05:54'),
(6,	'Potraviny',	'0',	'potraviny',	'2018-11-12 13:06:10'),
(7,	'Pečivo',	'6',	'pecivo',	'2018-11-12 13:27:07'),
(8,	'Mléčné výrobky',	'6',	'mlecne-vyrobky',	'2018-11-12 13:27:27'),
(9,	'Romány',	'5',	'romany',	'2018-11-12 13:27:53'),
(10,	'Dětské knihy',	'5',	'detske-knihy',	'2018-11-12 13:28:05'),
(11,	'Jogurty',	'8',	'jogurty',	'2018-11-12 13:28:45'),
(12,	'Bílé',	'11',	'bile',	'2018-11-12 13:29:12'),
(13,	'Ovocné',	'11',	'ovocne',	'2018-11-12 13:29:28');

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `active` bit(1) NOT NULL DEFAULT b'0',
  `slug` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `description` text COLLATE utf8_czech_ci NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `image` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `products` (`id`, `name`, `active`, `slug`, `category_id`, `description`, `price`, `image`, `color`, `created_at`, `updated_at`) VALUES
(13,	'The Mars Volta - Amputechture',	1,	'the-mars-volta-amputechture',	4,	'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Vivamus luctus egestas leo. Nunc dapibus tortor vel mi dapibus sollicitudin. Curabitur sagittis hendrerit ante. Et harum quidem rerum facilis est et expedita distinctio. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Donec vitae arcu. Et harum quidem rerum facilis est et expedita distinctio. Nullam at arcu a est sollicitudin euismod. Duis condimentum augue id magna semper rutrum. Phasellus faucibus molestie nisl. Pellentesque ipsum. Maecenas libero.',	799.00,	'amputechture.png',	NULL,	'2018-11-08 18:06:26',	'2018-11-12 16:23:04'),
(14,	'Gentle Giant - Acquiring the Taste',	1,	'gentle-giant-acquiring-the-taste',	4,	'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Vivamus luctus egestas leo. Nunc dapibus tortor vel mi dapibus sollicitudin. Curabitur sagittis hendrerit ante. Et harum quidem rerum facilis est et expedita distinctio. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Donec vitae arcu. Et harum quidem rerum facilis est et expedita distinctio. Nullam at arcu a est sollicitudin euismod. Duis condimentum augue id magna semper rutrum. Phasellus faucibus molestie nisl. Pellentesque ipsum. Maecenas libero.',	599.00,	'taste.jpg',	NULL,	'2018-11-08 18:06:26',	'2018-11-12 16:23:04'),
(15,	'At the drive-in - In/Casino/Out',	1,	'at-the-drive-in-in-casino-out',	4,	'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Vivamus luctus egestas leo. Nunc dapibus tortor vel mi dapibus sollicitudin. Curabitur sagittis hendrerit ante. Et harum quidem rerum facilis est et expedita distinctio. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Donec vitae arcu. Et harum quidem rerum facilis est et expedita distinctio. Nullam at arcu a est sollicitudin euismod. Duis condimentum augue id magna semper rutrum. Phasellus faucibus molestie nisl. Pellentesque ipsum. Maecenas libero.',	899.50,	'incasinoout.jpg',	NULL,	'2018-11-08 18:06:26',	'2018-11-12 16:23:04'),
(16,	'Louis Cole - Time',	1,	'louis-cole-time',	4,	'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Vivamus luctus egestas leo. Nunc dapibus tortor vel mi dapibus sollicitudin. Curabitur sagittis hendrerit ante. Et harum quidem rerum facilis est et expedita distinctio. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Donec vitae arcu. Et harum quidem rerum facilis est et expedita distinctio. Nullam at arcu a est sollicitudin euismod. Duis condimentum augue id magna semper rutrum. Phasellus faucibus molestie nisl. Pellentesque ipsum. Maecenas libero.',	599.00,	'placeholder.png',	NULL,	'2018-11-08 18:06:26',	'2018-11-12 16:13:38'),
(17,	'Knower - TIme',	1,	'knower',	4,	'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Vivamus luctus egestas leo. Nunc dapibus tortor vel mi dapibus sollicitudin. Curabitur sagittis hendrerit ante. Et harum quidem rerum facilis est et expedita distinctio. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Donec vitae arcu. Et harum quidem rerum facilis est et expedita distinctio. Nullam at arcu a est sollicitudin euismod. Duis condimentum augue id magna semper rutrum. Phasellus faucibus molestie nisl. Pellentesque ipsum. Maecenas libero.',	599.00,	'placeholder.png',	NULL,	'2018-11-08 18:07:06',	'2018-11-12 16:13:06'),
(18,	'Il Balleto di Bronzo - Ys',	1,	'il-balleto-di-bronzo-ys',	4,	'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Cras pede libero, dapibus nec, pretium sit amet, tempor quis. Vivamus luctus egestas leo. Nunc dapibus tortor vel mi dapibus sollicitudin. Curabitur sagittis hendrerit ante. Et harum quidem rerum facilis est et expedita distinctio. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Donec vitae arcu. Et harum quidem rerum facilis est et expedita distinctio. Nullam at arcu a est sollicitudin euismod. Duis condimentum augue id magna semper rutrum. Phasellus faucibus molestie nisl. Pellentesque ipsum. Maecenas libero.',	549.00,	'placeholder.png',	NULL,	'2018-11-08 18:16:46',	'2018-11-08 18:16:46'),
(24,	'Rohlík',	1,	'rohlik',	7,	'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas sollicitudin. Nulla non lectus sed nisl molestie malesuada. Nullam rhoncus aliquam metus. Donec vitae arcu. Maecenas libero. Nulla quis diam. Etiam bibendum elit eget erat. Nullam feugiat, turpis at pulvinar vulputate, erat libero tristique tellus, nec bibendum odio risus sit amet ante. Aenean vel massa quis mauris vehicula lacinia. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',	1.00,	'rohlik.jpg',	NULL,	'2018-11-12 16:23:50',	'2018-11-12 16:25:08'),
(25,	'Jogurt Olma bílý',	1,	'jogurt-olma-bily',	12,	'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas sollicitudin. Nulla non lectus sed nisl molestie malesuada. Nullam rhoncus aliquam metus. Donec vitae arcu. Maecenas libero. Nulla quis diam. Etiam bibendum elit eget erat. Nullam feugiat, turpis at pulvinar vulputate, erat libero tristique tellus, nec bibendum odio risus sit amet ante. Aenean vel massa quis mauris vehicula lacinia. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',	10.00,	'jogurt.jpg',	'bílý',	'2018-11-12 16:26:44',	'2018-11-12 16:27:02'),
(26,	'Quo Vadis - Henrik Sienkiewicz',	1,	'quo-vadis-henrik-sienkiewicz',	9,	'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas sollicitudin. Nulla non lectus sed nisl molestie malesuada. Nullam rhoncus aliquam metus. Donec vitae arcu. Maecenas libero. Nulla quis diam. Etiam bibendum elit eget erat. Nullam feugiat, turpis at pulvinar vulputate, erat libero tristique tellus, nec bibendum odio risus sit amet ante. Aenean vel massa quis mauris vehicula lacinia. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',	140.50,	'quo.jpg',	NULL,	'2018-11-12 16:29:39',	'2018-11-12 16:29:54'),
(27,	'Honzíkova Cesta',	1,	'honzikova-cesta',	10,	'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Integer pellentesque quam vel velit. Et harum quidem rerum facilis est et expedita distinctio. Fusce tellus. Fusce consectetuer risus a nunc. In sem justo, commodo ut, suscipit at, pharetra vitae, orci. Mauris suscipit, ligula sit amet pharetra semper, nibh ante cursus purus, vel sagittis velit mauris vel metus. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Praesent in mauris eu tortor porttitor accumsan. Fusce suscipit libero eget elit. In dapibus augue non sapien. Mauris elementum mauris vitae tortor.',	500.00,	'placeholder.png',	'',	'2018-11-12 17:53:02',	'2018-11-12 17:53:13');

DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `question` text COLLATE utf8_czech_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


-- 2018-11-12 18:04:59
